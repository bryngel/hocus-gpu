
SUBROUTINE Extrapolated_on_LEFT(primitive, Nx, Ny, Nz, nprims, NGh)

	implicit none
	
	integer :: i, j, k, npr
	integer, intent(IN) :: Nx, Ny, Nz, nprims, NGh
	double precision, intent(INOUT) :: primitive(-NGh+1:Nx+NGh,-NGh+1:Ny+NGh,-NGh+1:Nz+NGh,nprims)
	
	!!$acc update device(primitive)
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr=1,nprims
	DO k=1,Nz
	DO j=1,Ny
	DO i=1,NGh
		primitive(-i+1,j,k,npr) = primitive(1,j,k,npr)
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!!$acc update self(primitive)

END


SUBROUTINE Extrapolated_on_BOTTOM(primitive, Nx, Ny, Nz, nprims, NGh)

	implicit none
	
	integer :: i, j, k, npr
	integer, intent(IN) :: Nx, Ny, Nz, nprims, NGh
	double precision, intent(INOUT) :: primitive(-NGh+1:Nx+NGh,-NGh+1:Ny+NGh,-NGh+1:Nz+NGh,nprims)
	
	!!$acc update device(primitive)
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr=1,nprims
	DO k=1,Nz
	DO j=1,NGh
	DO i=1,Nx
		primitive(i,-j+1,k,npr) = primitive(i,1,k,npr)
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!!$acc update self(primitive)

END


SUBROUTINE Extrapolated_on_RIGHT(primitive, Nx, Ny, Nz, nprims, NGh)

	implicit none
	
	integer :: i, j, k, npr
	integer, intent(IN) :: Nx, Ny, Nz, nprims, NGh
	double precision, intent(INOUT) :: primitive(-NGh+1:Nx+NGh,-NGh+1:Ny+NGh,-NGh+1:Nz+NGh,nprims)
	
	!!$acc update device(primitive)
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr=1,nprims
	DO k=1,Nz
	DO j=1,Ny
	DO i=1,NGh
		primitive(Nx+i,j,k,npr) = primitive(Nx,j,k,npr)
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!!$acc update self(primitive)

END


SUBROUTINE Extrapolated_on_TOP(primitive, Nx, Ny, Nz, nprims, NGh)

	implicit none
	
	integer :: i, j, k, npr
	integer, intent(IN) :: Nx, Ny, Nz, nprims, NGh
	double precision, intent(INOUT) :: primitive(-NGh+1:Nx+NGh,-NGh+1:Ny+NGh,-NGh+1:Nz+NGh,nprims)
	
	!!$acc update device(primitive)
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr=1,nprims
	DO k=1,Nz
	DO j=1,NGh
	DO i=1,Nx
		primitive(i,Ny+j,k,npr) = primitive(i,Ny,k,npr)
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!!$acc update self(primitive)

END
