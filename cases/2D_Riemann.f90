
SUBROUTINE INPUTS()

	use global_vars
	implicit none
	
	print*, "Reading inputs..."
	
	! INPUTS
	nprims = 5				! [rho, u, v, w, P]
	nconserv = 5			! [rho, rho*u, rho*v, rho*w, E]
	NGh = 3					! No. of Ghost cells on either sides of boundaries
	CFL = 0.2d0
	rk_steps = 3
	
	! Fluid	
	Gamma = 1.4d0; Pr = 0.71d0
	
	write_freq = 500
	f2D = 1
		
	! Mesh (Riemann)
	Nx = 400
	Ny = 400
	Nz = 1
	
	t_end = 0.8d0
	
	!$acc enter data copyin(time_step, t_end, CFL)
	
END


SUBROUTINE GENERATE_GRID()

	use global_vars
	implicit none
	
	double precision :: xi, xf, yi, yf, zi, zf
	
	print*, "Generating grid..."
	
	! Geometry (Riemann)
	xi = 0.d0;	xf = 1.d0
	yi = 0.d0;	yf = 1.d0
	zi = 0.d0;	zf = 1.d0
	
	dx = (xf-xi)/Nx
	dy = (yf-yi)/Ny
	dz = (zf-zi)/Nz
	!$acc enter data copyin(dx,dy,dz)
	
	!Uniform grid
	DO i=1,Nx
		X_CC(i) = xi + (i - 0.5d0) * dx
	ENDDO
	
	DO j=1,Ny
		Y_CC(j) = yi + (j - 0.5d0) * dy
	ENDDO
	
	DO k=1,Nz
		Z_CC(j) = zi + (k - 0.5d0) * dz
	ENDDO
	
	!$acc update device(X_CC,Y_CC,Z_CC)
	
END


SUBROUTINE INITIALIZE()

	use global_vars
	implicit none
	
	double precision :: x,y,z, R, KE
	double precision, parameter :: Pi = 4.d0*ATan(1.d0)
	
	! for simulation resume
	! integer :: Nxin, Nyin, Nzin
	! double precision, dimension(Nxmax,NJmax,NKmax,nblocks,nprims) :: Qp_2d
	! integer, parameter :: fflow = 1
	! integer :: var
	
	print*, "Initializing variables..."
	
	! 2-D Riemann problem
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
	
		x = X_CC(i)
		y = Y_CC(j)
		
		IF (x>0.8d0 .and. y>0.8d0) THEN
		
			primitive(i,j,k,1) = 1.5d0					!rho_nd	
			primitive(i,j,k,2) = 0.d0 					!u_nd
			primitive(i,j,k,3) = 0.d0					!v_nd
			primitive(i,j,k,5) = 1.5d0					!P_nd
		
		ELSEIF (x<=0.8d0 .and. y>0.8d0) THEN
			
			primitive(i,j,k,1) = 33.d0/62.d0			!rho_nd	
			primitive(i,j,k,2) = 4.d0/SQRT(11.d0) 		!u_nd
			primitive(i,j,k,3) = 0.d0 					!v_nd
			primitive(i,j,k,5) = 0.3d0					!P_nd
		
		
		ELSEIF (x<=0.8d0 .and. y<=0.8d0) THEN
			
			primitive(i,j,k,1) = 77.d0/558.d0			!rho_nd	
			primitive(i,j,k,2) = 4.d0/SQRT(11.d0) 		!u_nd
			primitive(i,j,k,3) = 4.d0/SQRT(11.d0)		!v_nd
			primitive(i,j,k,5) = 9.d0/310.d0			!P_nd
		
		ELSEIF (x>0.8d0 .and. y<=0.8d0) THEN
			
			primitive(i,j,k,1) = 33.d0/62.d0			!rho_nd	
			primitive(i,j,k,2) = 0.d0 					!u_nd
			primitive(i,j,k,3) = 4.d0/SQRT(11.d0)		!v_nd
			primitive(i,j,k,5) = 0.3d0					!P_nd
		
		ENDIF
		
		primitive(i,j,k,4) = 0.d0		!w_nd
		
	ENDDO
	ENDDO
	ENDDO
	
	
	! prims to cons
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		
		!E =      P/(gamma-1) + Rho*(u**2 + v**2 + w**2)/2
		
		KE = 0.5d0*primitive(i,j,k,1)*(primitive(i,j,k,2)**2.d0 + primitive(i,j,k,3)**2.d0 + primitive(i,j,k,4)**2.d0)
		
		conservative(i,j,k,1) = primitive(i,j,k,1)
		conservative(i,j,k,2) = primitive(i,j,k,1) * primitive(i,j,k,2)
		conservative(i,j,k,3) = primitive(i,j,k,1) * primitive(i,j,k,3)
		conservative(i,j,k,4) = primitive(i,j,k,1) * primitive(i,j,k,4)
		conservative(i,j,k,5) = primitive(i,j,k,5)/(gamma-1) + KE
		
	ENDDO
	ENDDO
	ENDDO
	
	!$acc update device(primitive,conservative)
	
	call SET_PRIMITIVES()
	call OUTPUT(1)	! To write mesh and initialized solution

END


SUBROUTINE BOUNDARY_CONDITIONS()

	use global_vars
	implicit none
	
	call Extrapolated_on_TOP(primitive, Nx, Ny, Nz, nprims, NGh)
	call Extrapolated_on_BOTTOM(primitive, Nx, Ny, Nz, nprims, NGh)
	call Extrapolated_on_LEFT(primitive, Nx, Ny, Nz, nprims, NGh)
	call Extrapolated_on_RIGHT(primitive, Nx, Ny, Nz, nprims, NGh)
	
	! !$acc parallel loop gang vector collapse(3) default(present)
	! DO k = 1,Nz
	! DO j = 1,Ny
	! DO i = 1,NGh
	
		! Qp(1-i,j,k,:)  = Qp(1,j,k,:)			! Zero grad
		! Qp(Nx+i,j,k,:) = Qp(Nx,j,k,:)			! Zero grad
		
	! ENDDO
	! ENDDO
	! ENDDO
	
	! !$acc parallel loop gang vector collapse(3) default(present)
	! DO k = 1,Nz
	! DO j = 1,NGh
	! DO i = 1,Nx
	
		! Qp(i,1-j,k,:)  	= Qp(i,1,k,:)		! Zero grad
		! Qp(i,Ny+j,k,:) 	= Qp(i,Ny,k,:)		! Zero grad
		
	! ENDDO
	! ENDDO
	! ENDDO
	
END

