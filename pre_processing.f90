
SUBROUTINE ALLOCATE_VARS()

	use global_vars
	
	print*, "Allocating variables..."
	
	Nx_min = 1-NGh;	Nx_max = Nx+NGh
	Ny_min = 1-NGh;	Ny_max = Ny+NGh
	Nz_min = 1-NGh;	Nz_max = Nz+NGh
	!$acc enter data copyin(Nx, Ny, Nz, NGh, nprims, nconserv, Nx_min, Nx_max, Ny_min, Ny_max, Nz_min, Nz_max)
	
	! flow parameters
	!$acc enter data copyin(Gamma, Pr, Re, Mach, T_ref)
	
	! Cell averaged Conservative and Primitive variables
	ALLOCATE(primitive(Nx_min:Nx_max,Ny_min:Ny_max,Nz_min:Nz_max,nprims))
	ALLOCATE(conservative(Nx,Ny,Nz,nconserv))
	ALLOCATE(cons_ini(Nx,Ny,Nz,nconserv))
	ALLOCATE(Residual_RHS(Nx,Ny,Nz,nconserv))
	ALLOCATE(C5_TBV(Nx,Ny,Nz,nconserv))
	ALLOCATE(mp5TBV(Nx,Ny,Nz,nconserv))
	!$acc enter data create(primitive,conservative,cons_ini,Residual_RHS,C5_TBV,mp5TBV)
	
	! Variables on faces
	ALLOCATE(flux_ph(0:Nx,0:Ny,0:Nz,nprims))
	ALLOCATE(C5_phL(Nx_min:Nx_max,Ny_min:Ny_max,Nz_min:Nz_max,nprims))
	ALLOCATE(C5_phR(Nx_min:Nx_max,Ny_min:Ny_max,Nz_min:Nz_max,nprims))
	ALLOCATE(mp5phL(Nx_min:Nx_max,Ny_min:Ny_max,Nz_min:Nz_max,nprims))
	ALLOCATE(mp5phR(Nx_min:Nx_max,Ny_min:Ny_max,Nz_min:Nz_max,nprims))
	!$acc enter data create(flux_ph,C5_phL,C5_phR,mp5phL,mp5phR)
	
	! grid variables
	ALLOCATE(X_CC(Nx))
	ALLOCATE(Y_CC(Ny))
	ALLOCATE(Z_CC(Nz))
	!$acc enter data create(X_CC,Y_CC,Z_CC)
	
	! Temporal variables
	ALLOCATE(RK_factor1(rk_steps))
	ALLOCATE(RK_factor2(rk_steps))
	ALLOCATE(RK_factor3(rk_steps))
	!$acc enter data create(RK_factor1,RK_factor2,RK_factor3)
	
	! flags
	!$acc enter data copyin(f2D, fViscous, fVisc_coeff, Write_freq, fDervsReady)

END


SUBROUTINE SCHEME_COEFFICENTS()

	use global_vars
	implicit none
	
	! TVD RK3 coefficents
	RK_factor1(1) = 1.d0
	RK_factor1(2) = 3.d0/4.d0
	RK_factor1(3) = 1.d0/3.d0
	
	RK_factor2(1) = 0.d0
	RK_factor2(2) = 1.d0/4.d0
	RK_factor2(3) = 2.d0/3.d0
	
	RK_factor3(1) = 1.d0
	RK_factor3(2) = 1.d0/4.d0
	RK_factor3(3) = 2.d0/3.d0
	
	!$acc update device(RK_factor1,RK_factor2,RK_factor3)

END


