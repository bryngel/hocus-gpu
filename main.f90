
! BOGUS - A 3D cartisian Navier-Stokes solver
! "Best of Great Upwind Schemes"
! Team MIGHTY - Amareshwara Sainadh Chamarti, Hemanth Chandra Vamsi K, Natan Hoffmann, Sean Bokor and Steven Frankel
! Technion-Israel, Dec 2021

PROGRAM hack3D

	use global_vars
	
	! pre-processing
	call INPUTS()
	call ALLOCATE_VARS()
	call SCHEME_COEFFICENTS()
	call GENERATE_GRID()
	call INITIALIZE()
	
	! solver
	call CPU_TIME(start_time)
	DO WHILE (time<t_end)
	! DO iter = 1,2
	
		call RUNTIME_ROUTINES()
		
		DO step=1,rk_Steps
			call BOUNDARY_CONDITIONS()
			! call MP5_RECONSTRUCTION_WITH_PRIMS_CHARS()
			call HOCUS5_RECONSTRUCTION_WITH_PRIMS_CHARS()
			! viscous fluxes
			call TIME_INTEGRATION()
			call SET_PRIMITIVES()
		ENDDO
	
	ENDDO
	call CPU_TIME(end_time)
	
	
	! post processing
	call OUTPUT(1)
	print*, 'SOLUTION - at domain center'
	print*, primitive(Nx_max/2, Ny_max/2, 1,1)
	print*, primitive(Nx_max/2, Ny_max/2, 1,2)
	print*, primitive(Nx_max/2, Ny_max/2, 1,3)
	print*, primitive(Nx_max/2, Ny_max/2, 1,4)
	print*, primitive(Nx_max/2, Ny_max/2, 1,5)

END PROGRAM


SUBROUTINE RUNTIME_ROUTINES()
	
	use global_vars
	implicit none
	
	double precision :: mu_L, T, c
	
	IF(MOD(iter,write_freq) == 0) call OUTPUT(0)
	
	!!$acc update device(conservative)
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO ncr = 1,nconserv
    DO k = 1,Nz
    DO j = 1,Ny
    DO i = 1,Nx
		cons_ini(i,j,k,ncr) = conservative(i,j,k,ncr)
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!!$acc update self(cons_ini)
	
	time = time + time_step
	
	time_step = 1.d10
	
	IF (fViscous == 1) THEN
				
		!!$acc parallel loop gang vector collapse(3) reduction(min:time_step) default(present)
		DO k = 1,Nz
		DO j = 1,Ny
		DO i = 1,Nx
			
			IF (fVisc_coeff == 1) THEN
				! Temperature and viscosity at iph face (if velocity non-dimensionalized with 'U_inf-freestream Vel')
				T =  Gamma*Mach**2*primitive(i,j,k,5)/primitive(i,j,k,1)
				mu_L = (T**1.5d0)*(1.d0+110.4d0/T_ref)/(T+110.4d0/T_ref) /Re
				
			ELSEIF (fVisc_coeff == 2) THEN
				! Temperature and viscosity at iph face (if velocity non-dimensionalized with 'a- freestream speed of sound')
				T =  Gamma*primitive(i,j,k,5)/primitive(i,j,k,1)
				mu_L = (T**1.5d0)*(1.d0+110.4d0/T_ref)/(T+110.4d0/T_ref) *Mach/Re
				
			ELSEIF (fVisc_coeff == 3) THEN
				! Dimensiional solver
				mu_L = 1.d0/Re
			ENDIF
		
			c = SQRT(gamma*primitive(i,j,k,5)/primitive(i,j,k,1))
					
			time_step = MIN(time_step, &
							CFL*dx/(ABS(primitive(i,j,k,2))+c), CFL*dy/(ABS(primitive(i,j,k,3))+c), CFL*dz/(ABS(primitive(i,j,k,4))+c), &
							CFL*MIN(dx**2.0, dy**2.0, dz**2.0)/mu_L)
		ENDDO
		ENDDO
		ENDDO
	
	ELSE
		
		!$acc update device(time_step)
	
		!$acc parallel loop gang vector collapse(3) reduction(min:time_step) default(present)
		DO k = 1, Nz
		DO j = 1, Ny
		DO i = 1, Nx
			
			c = SQRT(gamma*primitive(i,j,k,5)/primitive(i,j,k,1))
					
			time_step = MIN(time_step, &
							CFL*dx/(ABS(primitive(i,j,k,2))+c), CFL*dy/(ABS(primitive(i,j,k,3))+c), CFL*dz/(ABS(primitive(i,j,k,4))+c))
			
		ENDDO
		ENDDO
		ENDDO
		
		!$acc update self(time_step)
	
	ENDIF		
	
	
	IF (time + time_step > t_end) time_step = t_end-time
	
	iter = iter +1
	
	WRITE(*,1) iter, "t =", time, "dt =", time_step
	1 format(i8, a6, f7.4, a7, ES15.8)
	
	
	IF (time + time_step<t_end .and. time_step < 1.d-9) then
		call OUTPUT(1)
		Print*, 'Total wall clock time = ', (end_time-start_time), 'secs'
		stop
	ENDIF

END


SUBROUTINE TIME_INTEGRATION()

	use global_vars
	
	!!$acc update device(cons_ini, conservative, Residual_RHS, time_step)
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO ncr = 1,nconserv
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		conservative(i,j,k,ncr) = RK_factor1(step)*cons_ini(i,j,k,ncr) &
								+ RK_factor2(step)*conservative(i,j,k,ncr) &
								+ RK_factor3(step)*Residual_RHS(i,j,k,ncr)*time_step
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!!$acc update self(conservative)

END


SUBROUTINE SET_PRIMITIVES()

	use global_vars
	
	!!$acc update device(conservative)
	
	!$acc parallel loop gang vector collapse(3) default(present)
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		
		! IF (conservative(i,j,k,1) < 0.d0) conservative(i,j,k,1) = 0.001d0
		
		primitive(i,j,k,1) =  conservative(i,j,k,1) !rho
		primitive(i,j,k,2) =  conservative(i,j,k,2)/conservative(i,j,k,1) !u
		primitive(i,j,k,3) =  conservative(i,j,k,3)/conservative(i,j,k,1) !v
		primitive(i,j,k,4) =  conservative(i,j,k,4)/conservative(i,j,k,1) !w
		primitive(i,j,k,5) = (conservative(i,j,k,5) - 0.5d0*conservative(i,j,k,1)*	&
						    ((conservative(i,j,k,2)/conservative(i,j,k,1))**2.d0 + (conservative(i,j,k,3)/conservative(i,j,k,1))**2.d0 &
						   + (conservative(i,j,k,4)/conservative(i,j,k,1))**2.d0))*(gamma-1.d0) !P
							
		! Clipping shit
		! IF (primitive(i,j,k,5) < 0.d0) primitive(i,j,k,5) = 0.001d0
		
	ENDDO
	ENDDO
	ENDDO

	!!$acc update self(primitive)

END


!================
! SOLVER ROUTINES
!================

SUBROUTINE MP5_RECONSTRUCTION_WITH_PRIMS_CHARS()

	! Task: Given 'Primitive vars- Qp' at cell centers, interpolate those values on to faces
	! Before interpolating Qp, Qp is transformed to Wp via d(Wp) = L*d(Qp) - characteristic variables

	use global_vars
	use Interpolation_routines
	use Riemann_solvers
	implicit none
	
	double precision :: nrx, nry, nrz
	double precision :: Qp_phL1, Qp_phL2, Qp_phL3, Qp_phL4, Qp_phL5
	double precision :: Qp_phR1, Qp_phR2, Qp_phR3, Qp_phR4, Qp_phR5
	
	double precision :: R_Eig11, R_Eig12, R_Eig13, R_Eig14, R_Eig15
	double precision :: R_Eig21, R_Eig23, R_Eig24, R_Eig25
	double precision :: R_Eig31, R_Eig32, R_Eig33, R_Eig35
	double precision :: R_Eig41, R_Eig42, R_Eig44, R_Eig45
	double precision :: R_Eig51, R_Eig55
	
	integer :: II
	double precision, dimension(nprims) :: W_phL, W_phR
	double precision, dimension(-2:3,nprims) :: Wp
    double precision :: p, c, sqrt_rho, divisor, rho
	
	
	!$acc data copyin(nrx,nry,nrz)
	
	nrx = 1.d0; nry = 0.d0; nrz = 0.d0
	!$acc parallel loop gang vector collapse(3) private(Wp,W_phL,W_phR) default(present)
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 0,Nx
		
		! Roe-averaged 'Rho' and 'c' on i+1/2 face
		sqrt_rho = sqrt(primitive(i+1,j,k,1)/primitive(i,j,k,1))
		rho      = sqrt(primitive(i,j,k,1)*primitive(i+1,j,k,1))
		divisor  = 1.d0/(sqrt_rho+1.d0)
		p   = (primitive(i,j,k,5) + (primitive(i+1,j,k,5)*sqrt_rho))*divisor
		c   = sqrt(gamma*p/rho)
		
		! Left and right Eigen vectors on i+1/2 face
		!--------------------------------------------
		!!$acc loop seq
		DO II=-2,3
			Wp(II,1) =    ( 0.d0     )*primitive(i+II,j,k,1) &
					    + (-nrx*rho*c)*primitive(i+II,j,k,2) &
					    + (-nry*rho*c)*primitive(i+II,j,k,3) &
					    + (-nrz*rho*c)*primitive(i+II,j,k,4) &
					    + ( 1.d0     )*primitive(i+II,j,k,5)
					  
			Wp(II,2) =  ( nrx*c**2.d0)*primitive(i+II,j,k,1) &
					  + ( 0.d0       )*primitive(i+II,j,k,2) &
					  + (-nrz        )*primitive(i+II,j,k,3) &
					  + ( nry        )*primitive(i+II,j,k,4) &
					  + (-nrx        )*primitive(i+II,j,k,5)
					  
			Wp(II,3) =  ( nrz*c**2.d0)*primitive(i+II,j,k,1) &
					  + (-nry        )*primitive(i+II,j,k,2) &
					  + ( nrx        )*primitive(i+II,j,k,3) &
					  + ( 0.d0       )*primitive(i+II,j,k,4) &
					  + (-nrz        )*primitive(i+II,j,k,5)
					  
			Wp(II,4) =  (-nry*c**2.d0)*primitive(i+II,j,k,1) &
					  + (-nrz        )*primitive(i+II,j,k,2) &
					  + ( 0.d0       )*primitive(i+II,j,k,3) &
					  + ( nrx        )*primitive(i+II,j,k,4) &
					  + ( nry        )*primitive(i+II,j,k,5)
					  
			Wp(II,5) =     (0.d0     )*primitive(i+II,j,k,1) &
					     + (nrx*rho*c)*primitive(i+II,j,k,2) &
					     + (nry*rho*c)*primitive(i+II,j,k,3) &
					     + (nrz*rho*c)*primitive(i+II,j,k,4) &
					     + (1.d0     )*primitive(i+II,j,k,5)
		ENDDO
		
		Call MP5_INTERPOLATE(Wp, W_phL, W_phR, nprims)
		
		! ------------------------------------------------------------------------------------------------------------------------------
		R_Eig11 =  0.5d0/c**2.d0  ;	R_Eig12 =  nrx/c**2.d0 ; R_Eig13 =  nrz/c**2.d0 ; R_Eig14 = -nry/c**2.d0 ; R_Eig15 = 0.5d0/c**2.d0	
		R_Eig21 = -0.5d0*nrx/rho/c;	R_Eig32 = -nrz         ; R_Eig23 = -nry         ; R_Eig24 = -nrz         ; R_Eig25 = 0.5d0*nrx/rho/c
		R_Eig31 = -0.5d0*nry/rho/c;	R_Eig42 =  nry         ; R_Eig33 =  nrx         ; R_Eig44 =  nrx         ; R_Eig35 = 0.5d0*nry/rho/c
		R_Eig41 = -0.5d0*nrz/rho/c;	                       ;                        ;                        ; R_Eig45 = 0.5d0*nrz/rho/c
		R_Eig51 =  0.5d0          ;	                       ;                        ;                        ; R_Eig55 = 0.5d0
		! ------------------------------------------------------------------------------------------------------------------------------
		
		Qp_phL1 = R_Eig11*W_phL(1) + R_Eig12*W_phL(2) + R_Eig13*W_phL(3) + R_Eig14*W_phL(4) + R_Eig15*W_phL(5)
		Qp_phL2 = R_Eig21*W_phL(1) + R_Eig23*W_phL(3) + R_Eig24*W_phL(4) + R_Eig25*W_phL(5)
		Qp_phL3 = R_Eig31*W_phL(1) + R_Eig32*W_phL(2) + R_Eig33*W_phL(3) + R_Eig35*W_phL(5)
		Qp_phL4 = R_Eig41*W_phL(1) + R_Eig42*W_phL(2) + R_Eig44*W_phL(4) + R_Eig45*W_phL(5)
		Qp_phL5 = R_Eig51*W_phL(1) + R_Eig55*W_phL(5)
		
		Qp_phR1 = R_Eig11*W_phR(1) + R_Eig12*W_phR(2) + R_Eig13*W_phR(3) + R_Eig14*W_phR(4) + R_Eig15*W_phR(5)
		Qp_phR2 = R_Eig21*W_phR(1) + R_Eig23*W_phR(3) + R_Eig24*W_phR(4) + R_Eig25*W_phR(5)
		Qp_phR3 = R_Eig31*W_phR(1) + R_Eig32*W_phR(2) + R_Eig33*W_phR(3) + R_Eig35*W_phR(5)
		Qp_phR4 = R_Eig41*W_phR(1) + R_Eig42*W_phR(2) + R_Eig44*W_phR(4) + R_Eig45*W_phR(5)
		Qp_phR5 = R_Eig51*W_phR(1) + R_Eig55*W_phR(5)
		
		call HLLC(Qp_phL1,Qp_phL2,Qp_phL3,Qp_phL4,Qp_phL5,&
				  Qp_phR1,Qp_phR2,Qp_phR3,Qp_phR4,Qp_phR5,&
				  nrx,nry,nrz,&
				  gamma,f2D,&
				  R_Eig11,R_Eig12,R_Eig13,R_Eig14,R_Eig15)
				  
		Flux_ph(i,j,k,1) = R_Eig11
		Flux_ph(i,j,k,2) = R_Eig12
		Flux_ph(i,j,k,3) = R_Eig13
		Flux_ph(i,j,k,4) = R_Eig14
		Flux_ph(i,j,k,5) = R_Eig15
		
	ENDDO
	ENDDO
	ENDDO
	
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO ncr = 1,nconserv
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		Residual_RHS(i,j,k,ncr) = (Flux_ph(i-1,j,k,ncr) - Flux_ph(i,j,k,ncr))/dx
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	
	nrx = 0.d0;	nry = 1.d0;	nrz = 0.d0
	!$acc parallel loop gang vector collapse(3) private(Wp,W_phL,W_phR) default(present)
	DO k = 1,Nz
	DO j = 0,Ny
	DO i = 1,Nx
		
		! Roe-averaged 'Rho' and 'c' on i+1/2 face
		sqrt_rho = sqrt(primitive(i,j+1,k,1)/primitive(i,j,k,1))
		rho      = sqrt(primitive(i,j,k,1)*primitive(i,j+1,k,1))
		divisor  = 1.d0/(sqrt_rho+1.d0)
		p   = (primitive(i,j,k,5) + (primitive(i,j+1,k,5)*sqrt_rho))*divisor
		c   = sqrt(gamma*p/rho)
		
		! Left and right Eigen vectors on i+1/2 face
		!--------------------------------------------
		!!$acc loop seq
		DO II=-2,3
			Wp(II,1) =    ( 0.d0     )*primitive(i,j+II,k,1) &
					    + (-nrx*rho*c)*primitive(i,j+II,k,2) &
					    + (-nry*rho*c)*primitive(i,j+II,k,3) &
					    + (-nrz*rho*c)*primitive(i,j+II,k,4) &
					    + ( 1.d0     )*primitive(i,j+II,k,5)
					                                
			Wp(II,2) =  ( nrx*c**2.d0)*primitive(i,j+II,k,1) &
					  + ( 0.d0       )*primitive(i,j+II,k,2) &
					  + (-nrz        )*primitive(i,j+II,k,3) &
					  + ( nry        )*primitive(i,j+II,k,4) &
					  + (-nrx        )*primitive(i,j+II,k,5)
					                                
			Wp(II,3) =  ( nrz*c**2.d0)*primitive(i,j+II,k,1) &
					  + (-nry        )*primitive(i,j+II,k,2) &
					  + ( nrx        )*primitive(i,j+II,k,3) &
					  + ( 0.d0       )*primitive(i,j+II,k,4) &
					  + (-nrz        )*primitive(i,j+II,k,5)
					                                
			Wp(II,4) =  (-nry*c**2.d0)*primitive(i,j+II,k,1) &
					  + (-nrz        )*primitive(i,j+II,k,2) &
					  + ( 0.d0       )*primitive(i,j+II,k,3) &
					  + ( nrx        )*primitive(i,j+II,k,4) &
					  + ( nry        )*primitive(i,j+II,k,5)
					                                
			Wp(II,5) =     (0.d0     )*primitive(i,j+II,k,1) &
					     + (nrx*rho*c)*primitive(i,j+II,k,2) &
					     + (nry*rho*c)*primitive(i,j+II,k,3) &
					     + (nrz*rho*c)*primitive(i,j+II,k,4) &
					     + (1.d0     )*primitive(i,j+II,k,5)
		ENDDO
		
		Call MP5_INTERPOLATE(Wp, W_phL, W_phR, nprims)
		
		! ------------------------------------------------------------------------------------------------------------------------------
		R_Eig11 =  0.5d0/c**2.d0  ;	R_Eig12 =  nrx/c**2.d0 ; R_Eig13 =  nrz/c**2.d0 ; R_Eig14 = -nry/c**2.d0 ; R_Eig15 = 0.5d0/c**2.d0	
		R_Eig21 = -0.5d0*nrx/rho/c;	R_Eig32 = -nrz         ; R_Eig23 = -nry         ; R_Eig24 = -nrz         ; R_Eig25 = 0.5d0*nrx/rho/c
		R_Eig31 = -0.5d0*nry/rho/c;	R_Eig42 =  nry         ; R_Eig33 =  nrx         ; R_Eig44 =  nrx         ; R_Eig35 = 0.5d0*nry/rho/c
		R_Eig41 = -0.5d0*nrz/rho/c;	                       ;                        ;                        ; R_Eig45 = 0.5d0*nrz/rho/c
		R_Eig51 =  0.5d0          ;	                       ;                        ;                        ; R_Eig55 = 0.5d0
		! ------------------------------------------------------------------------------------------------------------------------------
		
		Qp_phL1 = R_Eig11*W_phL(1) + R_Eig12*W_phL(2) + R_Eig13*W_phL(3) + R_Eig14*W_phL(4) + R_Eig15*W_phL(5)
		Qp_phL2 = R_Eig21*W_phL(1) + R_Eig23*W_phL(3) + R_Eig24*W_phL(4) + R_Eig25*W_phL(5)
		Qp_phL3 = R_Eig31*W_phL(1) + R_Eig32*W_phL(2) + R_Eig33*W_phL(3) + R_Eig35*W_phL(5)
		Qp_phL4 = R_Eig41*W_phL(1) + R_Eig42*W_phL(2) + R_Eig44*W_phL(4) + R_Eig45*W_phL(5)
		Qp_phL5 = R_Eig51*W_phL(1) + R_Eig55*W_phL(5)
		
		Qp_phR1 = R_Eig11*W_phR(1) + R_Eig12*W_phR(2) + R_Eig13*W_phR(3) + R_Eig14*W_phR(4) + R_Eig15*W_phR(5)
		Qp_phR2 = R_Eig21*W_phR(1) + R_Eig23*W_phR(3) + R_Eig24*W_phR(4) + R_Eig25*W_phR(5)
		Qp_phR3 = R_Eig31*W_phR(1) + R_Eig32*W_phR(2) + R_Eig33*W_phR(3) + R_Eig35*W_phR(5)
		Qp_phR4 = R_Eig41*W_phR(1) + R_Eig42*W_phR(2) + R_Eig44*W_phR(4) + R_Eig45*W_phR(5)
		Qp_phR5 = R_Eig51*W_phR(1) + R_Eig55*W_phR(5)
		
		call HLLC(Qp_phL1,Qp_phL2,Qp_phL3,Qp_phL4,Qp_phL5,&
				  Qp_phR1,Qp_phR2,Qp_phR3,Qp_phR4,Qp_phR5,&
				  nrx,nry,nrz,&
				  gamma,f2D,&
				  R_Eig11,R_Eig12,R_Eig13,R_Eig14,R_Eig15)
				  
		Flux_ph(i,j,k,1) = R_Eig11
		Flux_ph(i,j,k,2) = R_Eig12
		Flux_ph(i,j,k,3) = R_Eig13
		Flux_ph(i,j,k,4) = R_Eig14
		Flux_ph(i,j,k,5) = R_Eig15
		
	ENDDO
	ENDDO
	ENDDO
	
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO ncr = 1,nconserv
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		Residual_RHS(i,j,k,ncr) = Residual_RHS(i,j,k,ncr) + (Flux_ph(i,j-1,k,ncr) - Flux_ph(i,j,k,ncr))/dy
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	
	IF (f2D .ne. 1) THEN
	nrx = 0.d0;	nry = 0.d0;	nrz = 1.d0
	!$acc parallel loop gang vector collapse(3) private(Wp,W_phL,W_phR) default(present)
	DO k = 0,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		
		! Roe-averaged 'Rho' and 'c' on i+1/2 face
		sqrt_rho = sqrt(primitive(i,j,k+1,1)/primitive(i,j,k,1))
		rho      = sqrt(primitive(i,j,k,1)*primitive(i,j,k+1,1))
		divisor  = 1.d0/(sqrt_rho+1.d0)
		p   = (primitive(i,j,k,5) + (primitive(i,j,k+1,5)*sqrt_rho))*divisor
		c   = sqrt(gamma*p/rho)
		
		! Left and right Eigen vectors on i+1/2 face
		!--------------------------------------------
		!!$acc loop seq
		DO II=-2,3
			Wp(II,1) =    ( 0.d0     )*primitive(i,j,k+II,1) &
					    + (-nrx*rho*c)*primitive(i,j,k+II,2) &
					    + (-nry*rho*c)*primitive(i,j,k+II,3) &
					    + (-nrz*rho*c)*primitive(i,j,k+II,4) &
					    + ( 1.d0     )*primitive(i,j,k+II,5)
					                                  
			Wp(II,2) =  ( nrx*c**2.d0)*primitive(i,j,k+II,1) &
					  + ( 0.d0       )*primitive(i,j,k+II,2) &
					  + (-nrz        )*primitive(i,j,k+II,3) &
					  + ( nry        )*primitive(i,j,k+II,4) &
					  + (-nrx        )*primitive(i,j,k+II,5)
					                                  
			Wp(II,3) =  ( nrz*c**2.d0)*primitive(i,j,k+II,1) &
					  + (-nry        )*primitive(i,j,k+II,2) &
					  + ( nrx        )*primitive(i,j,k+II,3) &
					  + ( 0.d0       )*primitive(i,j,k+II,4) &
					  + (-nrz        )*primitive(i,j,k+II,5)
					                                  
			Wp(II,4) =  (-nry*c**2.d0)*primitive(i,j,k+II,1) &
					  + (-nrz        )*primitive(i,j,k+II,2) &
					  + ( 0.d0       )*primitive(i,j,k+II,3) &
					  + ( nrx        )*primitive(i,j,k+II,4) &
					  + ( nry        )*primitive(i,j,k+II,5)
					                                  
			Wp(II,5) =     (0.d0     )*primitive(i,j,k+II,1) &
					     + (nrx*rho*c)*primitive(i,j,k+II,2) &
					     + (nry*rho*c)*primitive(i,j,k+II,3) &
					     + (nrz*rho*c)*primitive(i,j,k+II,4) &
					     + (1.d0     )*primitive(i,j,k+II,5)
		ENDDO
		
		Call MP5_INTERPOLATE(Wp, W_phL, W_phR, nprims)
		
		! ------------------------------------------------------------------------------------------------------------------------------
		R_Eig11 =  0.5d0/c**2.d0  ;	R_Eig12 =  nrx/c**2.d0 ; R_Eig13 =  nrz/c**2.d0 ; R_Eig14 = -nry/c**2.d0 ; R_Eig15 = 0.5d0/c**2.d0	
		R_Eig21 = -0.5d0*nrx/rho/c;	R_Eig32 = -nrz         ; R_Eig23 = -nry         ; R_Eig24 = -nrz         ; R_Eig25 = 0.5d0*nrx/rho/c
		R_Eig31 = -0.5d0*nry/rho/c;	R_Eig42 =  nry         ; R_Eig33 =  nrx         ; R_Eig44 =  nrx         ; R_Eig35 = 0.5d0*nry/rho/c
		R_Eig41 = -0.5d0*nrz/rho/c;	                       ;                        ;                        ; R_Eig45 = 0.5d0*nrz/rho/c
		R_Eig51 =  0.5d0          ;	                       ;                        ;                        ; R_Eig55 = 0.5d0
		! ------------------------------------------------------------------------------------------------------------------------------
		
		Qp_phL1 = R_Eig11*W_phL(1) + R_Eig12*W_phL(2) + R_Eig13*W_phL(3) + R_Eig14*W_phL(4) + R_Eig15*W_phL(5)
		Qp_phL2 = R_Eig21*W_phL(1) + R_Eig23*W_phL(3) + R_Eig24*W_phL(4) + R_Eig25*W_phL(5)
		Qp_phL3 = R_Eig31*W_phL(1) + R_Eig32*W_phL(2) + R_Eig33*W_phL(3) + R_Eig35*W_phL(5)
		Qp_phL4 = R_Eig41*W_phL(1) + R_Eig42*W_phL(2) + R_Eig44*W_phL(4) + R_Eig45*W_phL(5)
		Qp_phL5 = R_Eig51*W_phL(1) + R_Eig55*W_phL(5)
		
		Qp_phR1 = R_Eig11*W_phR(1) + R_Eig12*W_phR(2) + R_Eig13*W_phR(3) + R_Eig14*W_phR(4) + R_Eig15*W_phR(5)
		Qp_phR2 = R_Eig21*W_phR(1) + R_Eig23*W_phR(3) + R_Eig24*W_phR(4) + R_Eig25*W_phR(5)
		Qp_phR3 = R_Eig31*W_phR(1) + R_Eig32*W_phR(2) + R_Eig33*W_phR(3) + R_Eig35*W_phR(5)
		Qp_phR4 = R_Eig41*W_phR(1) + R_Eig42*W_phR(2) + R_Eig44*W_phR(4) + R_Eig45*W_phR(5)
		Qp_phR5 = R_Eig51*W_phR(1) + R_Eig55*W_phR(5)
		
		call HLLC(Qp_phL1,Qp_phL2,Qp_phL3,Qp_phL4,Qp_phL5,&
				  Qp_phR1,Qp_phR2,Qp_phR3,Qp_phR4,Qp_phR5,&
				  nrx,nry,nrz,&
				  gamma,f2D,&
				  R_Eig11,R_Eig12,R_Eig13,R_Eig14,R_Eig15)
				  
		Flux_ph(i,j,k,1) = R_Eig11
		Flux_ph(i,j,k,2) = R_Eig12
		Flux_ph(i,j,k,3) = R_Eig13
		Flux_ph(i,j,k,4) = R_Eig14
		Flux_ph(i,j,k,5) = R_Eig15
		
	ENDDO
	ENDDO
	ENDDO
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO ncr = 1,nconserv
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		Residual_RHS(i,j,k,ncr) = Residual_RHS(i,j,k,ncr) + (Flux_ph(i,j,k-1,ncr) - Flux_ph(i,j,k,ncr))/dz
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	ENDIF
	
	!$acc end data
	
	
	! !$acc update self(Residual_RHS)

END


SUBROUTINE HOCUS5_RECONSTRUCTION_WITH_PRIMS_CHARS()

	! Task: Given 'Primitive vars- Qp' at cell centers, interpolate those values on to faces
	! Before interpolating Qp, Qp is transformed to Wp via d(Wp) = L*d(Qp) - characteristic variables

	use global_vars
	use Interpolation_routines
	use Riemann_solvers
	implicit none
	
	double precision :: nrx, nry, nrz
	double precision :: Qp_phL1, Qp_phL2, Qp_phL3, Qp_phL4, Qp_phL5
	double precision :: Qp_phR1, Qp_phR2, Qp_phR3, Qp_phR4, Qp_phR5
	
	double precision :: R_Eig11, R_Eig12, R_Eig13, R_Eig14, R_Eig15
	double precision :: R_Eig21, R_Eig23, R_Eig24, R_Eig25
	double precision :: R_Eig31, R_Eig32, R_Eig33, R_Eig35
	double precision :: R_Eig41, R_Eig42, R_Eig44, R_Eig45
	double precision :: R_Eig51, R_Eig55
	
	integer :: II
	double precision, dimension(nprims) :: W_phL, W_phR
	double precision, dimension(-2:3,nprims) :: Wp
    double precision :: p, c, sqrt_rho, divisor, rho
	
	! C5 interpolations
	call InterpolateX_C5L(primitive,C5_phL)
	call InterpolateX_C5R(primitive,C5_phR)
	
	! C6 interpolations
	! call InterpolateX_C6_Ctrl(primitive,C5_phL)
	! call InterpolateX_C6_Ctrl(primitive,C5_phR)
	
	
	!$acc data copyin(nrx,nry,nrz)
	! MP5 interpolations
	nrx = 1.d0; nry = 0.d0; nrz = 0.d0
	!$acc parallel loop gang vector collapse(3) private(Wp,W_phL,W_phR) default(present)
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 0,Nx
		
		! Roe-averaged 'Rho' and 'c' on i+1/2 face
		sqrt_rho = sqrt(primitive(i+1,j,k,1)/primitive(i,j,k,1))
		rho      = sqrt(primitive(i,j,k,1)*primitive(i+1,j,k,1))
		divisor  = 1.d0/(sqrt_rho+1.d0)
		p   = (primitive(i,j,k,5) + (primitive(i+1,j,k,5)*sqrt_rho))*divisor
		c   = sqrt(gamma*p/rho)
		
		! Left and right Eigen vectors on i+1/2 face
		!--------------------------------------------
		!!$acc loop seq
		DO II=-2,3
			Wp(II,1) =    ( 0.d0     )*primitive(i+II,j,k,1) &
					    + (-nrx*rho*c)*primitive(i+II,j,k,2) &
					    + (-nry*rho*c)*primitive(i+II,j,k,3) &
					    + (-nrz*rho*c)*primitive(i+II,j,k,4) &
					    + ( 1.d0     )*primitive(i+II,j,k,5)
					  
			Wp(II,2) =  ( nrx*c**2.d0)*primitive(i+II,j,k,1) &
					  + ( 0.d0       )*primitive(i+II,j,k,2) &
					  + (-nrz        )*primitive(i+II,j,k,3) &
					  + ( nry        )*primitive(i+II,j,k,4) &
					  + (-nrx        )*primitive(i+II,j,k,5)
					  
			Wp(II,3) =  ( nrz*c**2.d0)*primitive(i+II,j,k,1) &
					  + (-nry        )*primitive(i+II,j,k,2) &
					  + ( nrx        )*primitive(i+II,j,k,3) &
					  + ( 0.d0       )*primitive(i+II,j,k,4) &
					  + (-nrz        )*primitive(i+II,j,k,5)
					  
			Wp(II,4) =  (-nry*c**2.d0)*primitive(i+II,j,k,1) &
					  + (-nrz        )*primitive(i+II,j,k,2) &
					  + ( 0.d0       )*primitive(i+II,j,k,3) &
					  + ( nrx        )*primitive(i+II,j,k,4) &
					  + ( nry        )*primitive(i+II,j,k,5)
					  
			Wp(II,5) =     (0.d0     )*primitive(i+II,j,k,1) &
					     + (nrx*rho*c)*primitive(i+II,j,k,2) &
					     + (nry*rho*c)*primitive(i+II,j,k,3) &
					     + (nrz*rho*c)*primitive(i+II,j,k,4) &
					     + (1.d0     )*primitive(i+II,j,k,5)
		ENDDO
		
		Call MP5_INTERPOLATE(Wp, W_phL, W_phR, nprims)
		
		! ------------------------------------------------------------------------------------------------------------------------------
		R_Eig11 =  0.5d0/c**2.d0  ;	R_Eig12 =  nrx/c**2.d0 ; R_Eig13 =  nrz/c**2.d0 ; R_Eig14 = -nry/c**2.d0 ; R_Eig15 = 0.5d0/c**2.d0	
		R_Eig21 = -0.5d0*nrx/rho/c;	R_Eig32 = -nrz         ; R_Eig23 = -nry         ; R_Eig24 = -nrz         ; R_Eig25 = 0.5d0*nrx/rho/c
		R_Eig31 = -0.5d0*nry/rho/c;	R_Eig42 =  nry         ; R_Eig33 =  nrx         ; R_Eig44 =  nrx         ; R_Eig35 = 0.5d0*nry/rho/c
		R_Eig41 = -0.5d0*nrz/rho/c;	                       ;                        ;                        ; R_Eig45 = 0.5d0*nrz/rho/c
		R_Eig51 =  0.5d0          ;	                       ;                        ;                        ; R_Eig55 = 0.5d0
		! ------------------------------------------------------------------------------------------------------------------------------
		
		mp5phL(i,j,k,1) = R_Eig11*W_phL(1) + R_Eig12*W_phL(2) + R_Eig13*W_phL(3) + R_Eig14*W_phL(4) + R_Eig15*W_phL(5)
		mp5phL(i,j,k,2) = R_Eig21*W_phL(1) + R_Eig23*W_phL(3) + R_Eig24*W_phL(4) + R_Eig25*W_phL(5)
		mp5phL(i,j,k,3) = R_Eig31*W_phL(1) + R_Eig32*W_phL(2) + R_Eig33*W_phL(3) + R_Eig35*W_phL(5)
		mp5phL(i,j,k,4) = R_Eig41*W_phL(1) + R_Eig42*W_phL(2) + R_Eig44*W_phL(4) + R_Eig45*W_phL(5)
		mp5phL(i,j,k,5) = R_Eig51*W_phL(1) + R_Eig55*W_phL(5)
		
		mp5phR(i,j,k,1) = R_Eig11*W_phR(1) + R_Eig12*W_phR(2) + R_Eig13*W_phR(3) + R_Eig14*W_phR(4) + R_Eig15*W_phR(5)
		mp5phR(i,j,k,2) = R_Eig21*W_phR(1) + R_Eig23*W_phR(3) + R_Eig24*W_phR(4) + R_Eig25*W_phR(5)
		mp5phR(i,j,k,3) = R_Eig31*W_phR(1) + R_Eig32*W_phR(2) + R_Eig33*W_phR(3) + R_Eig35*W_phR(5)
		mp5phR(i,j,k,4) = R_Eig41*W_phR(1) + R_Eig42*W_phR(2) + R_Eig44*W_phR(4) + R_Eig45*W_phR(5)
		mp5phR(i,j,k,5) = R_Eig51*W_phR(1) + R_Eig55*W_phR(5)
		
	ENDDO
	ENDDO
	ENDDO
	
	! Compute TBVs
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		mp5TBV(i,j,k,npr) = ABS(mp5phL(i-1,j,k,npr) - mp5phR(i-1,j,k,npr)) + &
							ABS(mp5phL(i+0,j,k,npr) - mp5phR(i+0,j,k,npr))
		C5_TBV(i,j,k,npr) = ABS(C5_phL(i-1,j,k,npr) - C5_phR(i-1,j,k,npr)) + &
							ABS(C5_phL(i+0,j,k,npr) - C5_phR(i+0,j,k,npr))
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		IF (mp5TBV(i,j,k,npr) < C5_TBV(i,j,k,npr)) THEN
			C5_phL(i-2:i+1,j,k,npr) = mp5phL(i-2:i+1,j,k,npr)
			C5_phR(i-2:i+1,j,k,npr) = mp5phR(i-2:i+1,j,k,npr)
		ENDIF
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!$acc parallel loop gang vector collapse(3) default(present)
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 0,Nx
		Qp_phL1 = C5_phL(i,j,k,1)
		Qp_phL2 = C5_phL(i,j,k,2)
		Qp_phL3 = C5_phL(i,j,k,3)
		Qp_phL4 = C5_phL(i,j,k,4)
		Qp_phL5 = C5_phL(i,j,k,5)
		
		Qp_phR1 = C5_phR(i,j,k,1)
		Qp_phR2 = C5_phR(i,j,k,2)
		Qp_phR3 = C5_phR(i,j,k,3)
		Qp_phR4 = C5_phR(i,j,k,4)
		Qp_phR5 = C5_phR(i,j,k,5)
		
		call HLLC(Qp_phL1,Qp_phL2,Qp_phL3,Qp_phL4,Qp_phL5,&
				  Qp_phR1,Qp_phR2,Qp_phR3,Qp_phR4,Qp_phR5,&
				  nrx,nry,nrz,&
				  gamma,f2D,&
				  R_Eig11,R_Eig12,R_Eig13,R_Eig14,R_Eig15)
				  
		Flux_ph(i,j,k,1) = R_Eig11
		Flux_ph(i,j,k,2) = R_Eig12
		Flux_ph(i,j,k,3) = R_Eig13
		Flux_ph(i,j,k,4) = R_Eig14
		Flux_ph(i,j,k,5) = R_Eig15
	ENDDO
	ENDDO
	ENDDO
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO ncr = 1,nconserv
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		Residual_RHS(i,j,k,ncr) = (Flux_ph(i-1,j,k,ncr) - Flux_ph(i,j,k,ncr))/dx
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	
	! Y direction
	call InterpolateY_C5L(primitive,C5_phL)
	call InterpolateY_C5R(primitive,C5_phR)
	
	! C6 interpolations
	! call InterpolateY_C6_Ctrl(primitive,C5_phL)
	! call InterpolateY_C6_Ctrl(primitive,C5_phR)
	
	nrx = 0.d0;	nry = 1.d0;	nrz = 0.d0
	!$acc parallel loop gang vector collapse(3) private(Wp,W_phL,W_phR) default(present)
	DO k = 1,Nz
	DO j = 0,Ny
	DO i = 1,Nx
		
		! Roe-averaged 'Rho' and 'c' on i+1/2 face
		sqrt_rho = sqrt(primitive(i,j+1,k,1)/primitive(i,j,k,1))
		rho      = sqrt(primitive(i,j,k,1)*primitive(i,j+1,k,1))
		divisor  = 1.d0/(sqrt_rho+1.d0)
		p   = (primitive(i,j,k,5) + (primitive(i,j+1,k,5)*sqrt_rho))*divisor
		c   = sqrt(gamma*p/rho)
		
		! Left and right Eigen vectors on i+1/2 face
		!--------------------------------------------
		!!$acc loop seq
		DO II=-2,3
			Wp(II,1) =    ( 0.d0     )*primitive(i,j+II,k,1) &
					    + (-nrx*rho*c)*primitive(i,j+II,k,2) &
					    + (-nry*rho*c)*primitive(i,j+II,k,3) &
					    + (-nrz*rho*c)*primitive(i,j+II,k,4) &
					    + ( 1.d0     )*primitive(i,j+II,k,5)
					                                
			Wp(II,2) =  ( nrx*c**2.d0)*primitive(i,j+II,k,1) &
					  + ( 0.d0       )*primitive(i,j+II,k,2) &
					  + (-nrz        )*primitive(i,j+II,k,3) &
					  + ( nry        )*primitive(i,j+II,k,4) &
					  + (-nrx        )*primitive(i,j+II,k,5)
					                                
			Wp(II,3) =  ( nrz*c**2.d0)*primitive(i,j+II,k,1) &
					  + (-nry        )*primitive(i,j+II,k,2) &
					  + ( nrx        )*primitive(i,j+II,k,3) &
					  + ( 0.d0       )*primitive(i,j+II,k,4) &
					  + (-nrz        )*primitive(i,j+II,k,5)
					                                
			Wp(II,4) =  (-nry*c**2.d0)*primitive(i,j+II,k,1) &
					  + (-nrz        )*primitive(i,j+II,k,2) &
					  + ( 0.d0       )*primitive(i,j+II,k,3) &
					  + ( nrx        )*primitive(i,j+II,k,4) &
					  + ( nry        )*primitive(i,j+II,k,5)
					                                
			Wp(II,5) =     (0.d0     )*primitive(i,j+II,k,1) &
					     + (nrx*rho*c)*primitive(i,j+II,k,2) &
					     + (nry*rho*c)*primitive(i,j+II,k,3) &
					     + (nrz*rho*c)*primitive(i,j+II,k,4) &
					     + (1.d0     )*primitive(i,j+II,k,5)
		ENDDO
		
		Call MP5_INTERPOLATE(Wp, W_phL, W_phR, nprims)
		
		! ------------------------------------------------------------------------------------------------------------------------------
		R_Eig11 =  0.5d0/c**2.d0  ;	R_Eig12 =  nrx/c**2.d0 ; R_Eig13 =  nrz/c**2.d0 ; R_Eig14 = -nry/c**2.d0 ; R_Eig15 = 0.5d0/c**2.d0	
		R_Eig21 = -0.5d0*nrx/rho/c;	R_Eig32 = -nrz         ; R_Eig23 = -nry         ; R_Eig24 = -nrz         ; R_Eig25 = 0.5d0*nrx/rho/c
		R_Eig31 = -0.5d0*nry/rho/c;	R_Eig42 =  nry         ; R_Eig33 =  nrx         ; R_Eig44 =  nrx         ; R_Eig35 = 0.5d0*nry/rho/c
		R_Eig41 = -0.5d0*nrz/rho/c;	                       ;                        ;                        ; R_Eig45 = 0.5d0*nrz/rho/c
		R_Eig51 =  0.5d0          ;	                       ;                        ;                        ; R_Eig55 = 0.5d0
		! ------------------------------------------------------------------------------------------------------------------------------
		
		mp5phL(i,j,k,1) = R_Eig11*W_phL(1) + R_Eig12*W_phL(2) + R_Eig13*W_phL(3) + R_Eig14*W_phL(4) + R_Eig15*W_phL(5)
		mp5phL(i,j,k,2) = R_Eig21*W_phL(1) + R_Eig23*W_phL(3) + R_Eig24*W_phL(4) + R_Eig25*W_phL(5)
		mp5phL(i,j,k,3) = R_Eig31*W_phL(1) + R_Eig32*W_phL(2) + R_Eig33*W_phL(3) + R_Eig35*W_phL(5)
		mp5phL(i,j,k,4) = R_Eig41*W_phL(1) + R_Eig42*W_phL(2) + R_Eig44*W_phL(4) + R_Eig45*W_phL(5)
		mp5phL(i,j,k,5) = R_Eig51*W_phL(1) + R_Eig55*W_phL(5)
		
		mp5phR(i,j,k,1) = R_Eig11*W_phR(1) + R_Eig12*W_phR(2) + R_Eig13*W_phR(3) + R_Eig14*W_phR(4) + R_Eig15*W_phR(5)
		mp5phR(i,j,k,2) = R_Eig21*W_phR(1) + R_Eig23*W_phR(3) + R_Eig24*W_phR(4) + R_Eig25*W_phR(5)
		mp5phR(i,j,k,3) = R_Eig31*W_phR(1) + R_Eig32*W_phR(2) + R_Eig33*W_phR(3) + R_Eig35*W_phR(5)
		mp5phR(i,j,k,4) = R_Eig41*W_phR(1) + R_Eig42*W_phR(2) + R_Eig44*W_phR(4) + R_Eig45*W_phR(5)
		mp5phR(i,j,k,5) = R_Eig51*W_phR(1) + R_Eig55*W_phR(5)
		
	ENDDO
	ENDDO
	ENDDO
	
	! Compute TBVs
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		mp5TBV(i,j,k,npr) = ABS(mp5phL(i,j-1,k,npr) - mp5phR(i,j-1,k,npr)) + &
							ABS(mp5phL(i,j+0,k,npr) - mp5phR(i,j+0,k,npr))
		C5_TBV(i,j,k,npr) = ABS(C5_phL(i,j-1,k,npr) - C5_phR(i,j-1,k,npr)) + &
							ABS(C5_phL(i,j+0,k,npr) - C5_phR(i,j+0,k,npr))
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		IF (mp5TBV(i,j,k,npr) < C5_TBV(i,j,k,npr)) THEN
			C5_phL(i,j-2:j+1,k,npr) = mp5phL(i,j-2:j+1,k,npr)
			C5_phR(i,j-2:j+1,k,npr) = mp5phR(i,j-2:j+1,k,npr)
		ENDIF
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!$acc parallel loop gang vector collapse(3) default(present)
	DO k = 1,Nz
	DO j = 0,Ny
	DO i = 1,Nx
		Qp_phL1 = C5_phL(i,j,k,1)
		Qp_phL2 = C5_phL(i,j,k,2)
		Qp_phL3 = C5_phL(i,j,k,3)
		Qp_phL4 = C5_phL(i,j,k,4)
		Qp_phL5 = C5_phL(i,j,k,5)
		          
		Qp_phR1 = C5_phR(i,j,k,1)
		Qp_phR2 = C5_phR(i,j,k,2)
		Qp_phR3 = C5_phR(i,j,k,3)
		Qp_phR4 = C5_phR(i,j,k,4)
		Qp_phR5 = C5_phR(i,j,k,5)
		
		call HLLC(Qp_phL1,Qp_phL2,Qp_phL3,Qp_phL4,Qp_phL5,&
				  Qp_phR1,Qp_phR2,Qp_phR3,Qp_phR4,Qp_phR5,&
				  nrx,nry,nrz,&
				  gamma,f2D,&
				  R_Eig11,R_Eig12,R_Eig13,R_Eig14,R_Eig15)
			  
		Flux_ph(i,j,k,1) = R_Eig11
		Flux_ph(i,j,k,2) = R_Eig12
		Flux_ph(i,j,k,3) = R_Eig13
		Flux_ph(i,j,k,4) = R_Eig14
		Flux_ph(i,j,k,5) = R_Eig15
	ENDDO
	ENDDO
	ENDDO
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO ncr = 1,nconserv
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		Residual_RHS(i,j,k,ncr) = Residual_RHS(i,j,k,ncr) + (Flux_ph(i,j-1,k,ncr) - Flux_ph(i,j,k,ncr))/dy
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	
	IF (f2D .ne. 1) THEN
	
	call InterpolateZ_C5L(primitive,C5_phL)
	call InterpolateZ_C5R(primitive,C5_phR)
	
	! C6 interpolations
	! call InterpolateZ_C6_Ctrl(primitive,C5_phL)
	! call InterpolateZ_C6_Ctrl(primitive,C5_phR)
	
	nrx = 0.d0;	nry = 0.d0;	nrz = 1.d0
	!$acc parallel loop gang vector collapse(3) private(Wp,W_phL,W_phR) default(present)
	DO k = 0,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		
		! Roe-averaged 'Rho' and 'c' on i+1/2 face
		sqrt_rho = sqrt(primitive(i,j,k+1,1)/primitive(i,j,k,1))
		rho      = sqrt(primitive(i,j,k,1)*primitive(i,j,k+1,1))
		divisor  = 1.d0/(sqrt_rho+1.d0)
		p   = (primitive(i,j,k,5) + (primitive(i,j,k+1,5)*sqrt_rho))*divisor
		c   = sqrt(gamma*p/rho)
		
		! Left and right Eigen vectors on i+1/2 face
		!--------------------------------------------
		!!$acc loop seq
		DO II=-2,3
			Wp(II,1) =    ( 0.d0     )*primitive(i,j,k+II,1) &
					    + (-nrx*rho*c)*primitive(i,j,k+II,2) &
					    + (-nry*rho*c)*primitive(i,j,k+II,3) &
					    + (-nrz*rho*c)*primitive(i,j,k+II,4) &
					    + ( 1.d0     )*primitive(i,j,k+II,5)
			
			Wp(II,2) =  ( nrx*c**2.d0)*primitive(i,j,k+II,1) &
					  + ( 0.d0       )*primitive(i,j,k+II,2) &
					  + (-nrz        )*primitive(i,j,k+II,3) &
					  + ( nry        )*primitive(i,j,k+II,4) &
					  + (-nrx        )*primitive(i,j,k+II,5)
					                                  
			Wp(II,3) =  ( nrz*c**2.d0)*primitive(i,j,k+II,1) &
					  + (-nry        )*primitive(i,j,k+II,2) &
					  + ( nrx        )*primitive(i,j,k+II,3) &
					  + ( 0.d0       )*primitive(i,j,k+II,4) &
					  + (-nrz        )*primitive(i,j,k+II,5)
					                                  
			Wp(II,4) =  (-nry*c**2.d0)*primitive(i,j,k+II,1) &
					  + (-nrz        )*primitive(i,j,k+II,2) &
					  + ( 0.d0       )*primitive(i,j,k+II,3) &
					  + ( nrx        )*primitive(i,j,k+II,4) &
					  + ( nry        )*primitive(i,j,k+II,5)
					                                  
			Wp(II,5) =     (0.d0     )*primitive(i,j,k+II,1) &
					     + (nrx*rho*c)*primitive(i,j,k+II,2) &
					     + (nry*rho*c)*primitive(i,j,k+II,3) &
					     + (nrz*rho*c)*primitive(i,j,k+II,4) &
					     + (1.d0     )*primitive(i,j,k+II,5)
		ENDDO
		
		Call MP5_INTERPOLATE(Wp, W_phL, W_phR, nprims)
		
		! ------------------------------------------------------------------------------------------------------------------------------
		R_Eig11 =  0.5d0/c**2.d0  ;	R_Eig12 =  nrx/c**2.d0 ; R_Eig13 =  nrz/c**2.d0 ; R_Eig14 = -nry/c**2.d0 ; R_Eig15 = 0.5d0/c**2.d0	
		R_Eig21 = -0.5d0*nrx/rho/c;	R_Eig32 = -nrz         ; R_Eig23 = -nry         ; R_Eig24 = -nrz         ; R_Eig25 = 0.5d0*nrx/rho/c
		R_Eig31 = -0.5d0*nry/rho/c;	R_Eig42 =  nry         ; R_Eig33 =  nrx         ; R_Eig44 =  nrx         ; R_Eig35 = 0.5d0*nry/rho/c
		R_Eig41 = -0.5d0*nrz/rho/c;	                       ;                        ;                        ; R_Eig45 = 0.5d0*nrz/rho/c
		R_Eig51 =  0.5d0          ;	                       ;                        ;                        ; R_Eig55 = 0.5d0
		! ------------------------------------------------------------------------------------------------------------------------------
		
		mp5phL(i,j,k,1) = R_Eig11*W_phL(1) + R_Eig12*W_phL(2) + R_Eig13*W_phL(3) + R_Eig14*W_phL(4) + R_Eig15*W_phL(5)
		mp5phL(i,j,k,2) = R_Eig21*W_phL(1) + R_Eig23*W_phL(3) + R_Eig24*W_phL(4) + R_Eig25*W_phL(5)
		mp5phL(i,j,k,3) = R_Eig31*W_phL(1) + R_Eig32*W_phL(2) + R_Eig33*W_phL(3) + R_Eig35*W_phL(5)
		mp5phL(i,j,k,4) = R_Eig41*W_phL(1) + R_Eig42*W_phL(2) + R_Eig44*W_phL(4) + R_Eig45*W_phL(5)
		mp5phL(i,j,k,5) = R_Eig51*W_phL(1) + R_Eig55*W_phL(5)
		
		mp5phR(i,j,k,1) = R_Eig11*W_phR(1) + R_Eig12*W_phR(2) + R_Eig13*W_phR(3) + R_Eig14*W_phR(4) + R_Eig15*W_phR(5)
		mp5phR(i,j,k,2) = R_Eig21*W_phR(1) + R_Eig23*W_phR(3) + R_Eig24*W_phR(4) + R_Eig25*W_phR(5)
		mp5phR(i,j,k,3) = R_Eig31*W_phR(1) + R_Eig32*W_phR(2) + R_Eig33*W_phR(3) + R_Eig35*W_phR(5)
		mp5phR(i,j,k,4) = R_Eig41*W_phR(1) + R_Eig42*W_phR(2) + R_Eig44*W_phR(4) + R_Eig45*W_phR(5)
		mp5phR(i,j,k,5) = R_Eig51*W_phR(1) + R_Eig55*W_phR(5)
		
	ENDDO
	ENDDO
	ENDDO
	
	! Compute TBVs
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		mp5TBV(i,j,k,npr) = ABS(mp5phL(i,j,k-1,npr) - mp5phR(i,j,k-1,npr)) + &
							ABS(mp5phL(i,j,k+0,npr) - mp5phR(i,j,k+0,npr))
		C5_TBV(i,j,k,npr) = ABS(C5_phL(i,j,k-1,npr) - C5_phR(i,j,k-1,npr)) + &
							ABS(C5_phL(i,j,k+0,npr) - C5_phR(i,j,k+0,npr))
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		IF (mp5TBV(i,j,k,npr) < C5_TBV(i,j,k,npr)) THEN
			C5_phL(i,j,k-2:k+1,npr) = mp5phL(i,j,k-2:k+1,npr)
			C5_phR(i,j,k-2:k+1,npr) = mp5phR(i,j,k-2:k+1,npr)
		ENDIF
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!$acc parallel loop gang vector collapse(3) default(present)
	DO k = 0,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		Qp_phL1 = C5_phL(i,j,k,1)
		Qp_phL2 = C5_phL(i,j,k,2)
		Qp_phL3 = C5_phL(i,j,k,3)
		Qp_phL4 = C5_phL(i,j,k,4)
		Qp_phL5 = C5_phL(i,j,k,5)
		          
		Qp_phR1 = C5_phR(i,j,k,1)
		Qp_phR2 = C5_phR(i,j,k,2)
		Qp_phR3 = C5_phR(i,j,k,3)
		Qp_phR4 = C5_phR(i,j,k,4)
		Qp_phR5 = C5_phR(i,j,k,5)
		
		call HLLC(Qp_phL1,Qp_phL2,Qp_phL3,Qp_phL4,Qp_phL5,&
				  Qp_phR1,Qp_phR2,Qp_phR3,Qp_phR4,Qp_phR5,&
				  nrx,nry,nrz,&
				  gamma,f2D,&
				  R_Eig11,R_Eig12,R_Eig13,R_Eig14,R_Eig15)
				  
		Flux_ph(i,j,k,1) = R_Eig11
		Flux_ph(i,j,k,2) = R_Eig12
		Flux_ph(i,j,k,3) = R_Eig13
		Flux_ph(i,j,k,4) = R_Eig14
		Flux_ph(i,j,k,5) = R_Eig15
	ENDDO
	ENDDO
	ENDDO
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO ncr = 1,nconserv
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		Residual_RHS(i,j,k,ncr) = Residual_RHS(i,j,k,ncr) + (Flux_ph(i,j,k-1,ncr) - Flux_ph(i,j,k,ncr))/dz
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	ENDIF
	
	!$acc end data
END


!==================
! C5 INTERPOLATIONS
!==================

! Left face
SUBROUTINE InterpolateX_C5L(PHI,PHI_L)

	use global_vars
	use minmodfunctions
	implicit none
	
	integer :: n, SPt, EPt
	double precision, dimension(0:Nx) :: cn, dn
	
	double precision, dimension(Nx_min:Nx_max,Ny_min:Ny_max,Nz_min:Nz_max,nprims) :: PHI, PHI_L
	
	double precision :: alpha_a, alpha_c
	double precision, dimension(0:Nx) :: aa_tdma, cc_tdma
	
	alpha_a = 1.d0/2.d0;	alpha_c = 1.d0/6.d0
	
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 1*(f2D),Nz
	DO j = 0,Ny
	DO i = 0,Nx
		
		IF (i>=1 .and. i<=Nx-1) THEN
			PHI_L(i,j,k,npr) = 1.d0/18.d0*PHI(i-1,j,k,npr) &
							+ 19.d0/18.d0*PHI(i+0,j,k,npr) &
							  + 5.d0/9.d0*PHI(i+1,j,k,npr)
		ELSEIF (i == 0 .or. i==Nx) THEN
			PHI_L(i,j,k,npr) = (2.d0*PHI(i-2,j,k,npr) &
							 - 13.d0*PHI(i-1,j,k,npr) &
							 + 47.d0*PHI(i+0,j,k,npr) &
							 + 27.d0*PHI(i+1,j,k,npr) &
							  - 3.d0*PHI(i+2,j,k,npr))/60.d0
		ENDIF
	
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!$acc data copyin(aa_tdma,cc_tdma)
	!$acc parallel loop default(present)
	DO i = 0,Nx
		if (i .ge. 0 .and. i .le. Nx-1) THEN
			aa_tdma(i) = alpha_a
			cc_tdma(i) = alpha_c
		elseif (i == 0 .or. i == Nx) THEN
			aa_tdma(i) = 0.d0
			cc_tdma(i) = 0.d0
		endif
	ENDDO
	
	!$acc parallel loop gang vector collapse(3) private(cn,dn) default(present)
	DO npr = 1,nprims
	DO k = 1*(f2D),Nz
	DO j = 0,Ny
		
		SPt = 0;	EPt = Nx
		
		cn(SPt) = cc_tdma(SPt)
		dn(SPt) = PHI_L(SPt,j,k,npr)
		
		!$acc loop seq
		DO n = SPt+1, EPt-1
			cn(n) = cc_tdma(n) / (1.d0 - cn(n-1) * aa_tdma(n))
		ENDDO
		
		!$acc loop seq
		DO n = SPt+1, EPt
			dn(n) = (PHI_L(n,j,k,npr) - dn(n-1) * aa_tdma(n)) / (1.d0- cn(n-1) * aa_tdma(n))
		ENDDO
		
		PHI_L(EPt,j,k,npr) = dn(EPt)
		
		!$acc loop seq
		DO n = EPt-1,SPt,-1
			PHI_L(n,j,k,npr) = dn(n) - cn(n) * PHI_L(n+1,j,k,npr)
		ENDDO
		
	ENDDO
	ENDDO
	ENDDO
	!$acc end data
	
END


SUBROUTINE InterpolateY_C5L(PHI,PHI_L)

	use global_vars
	use minmodfunctions
	implicit none
	
	integer :: n, SPt, EPt
	double precision, dimension(0:Ny) :: cn, dn
	
	double precision, dimension(Nx_min:Nx_max,Ny_min:Ny_max,Nz_min:Nz_max,nprims) :: PHI, PHI_L
	
	double precision :: alpha_a, alpha_c
	double precision, dimension(0:Ny) :: aa_tdma, cc_tdma
	
	alpha_a = 1.d0/2.d0;	alpha_c = 1.d0/6.d0
	

	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 1*(f2D),Nz
	DO j = 0,Ny
	DO i = 0,Nx
		
		IF (j>=1 .and. j<=Ny-1) THEN
			PHI_L(i,j,k,npr) = 1.d0/18.d0*PHI(i,j-1,k,npr) &
							+ 19.d0/18.d0*PHI(i,j+0,k,npr) &
							  + 5.d0/9.d0*PHI(i,j+1,k,npr)
		ELSEIF (j == 0 .or. j==Ny) THEN
			PHI_L(i,j,k,npr) = (2.d0*PHI(i,j-2,k,npr) &
							 - 13.d0*PHI(i,j-1,k,npr) &
							 + 47.d0*PHI(i,j+0,k,npr) &
							 + 27.d0*PHI(i,j+1,k,npr) &
							  - 3.d0*PHI(i,j+2,k,npr))/60.d0
		ENDIF
	
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!$acc data copyin(aa_tdma,cc_tdma)
	!$acc parallel loop default(present)
	DO i = 0,Ny
		if (i .ge. 0 .and. i .le. Ny-1) THEN
			aa_tdma(i) = alpha_a
			cc_tdma(i) = alpha_c
		elseif (i == 0 .or. i == Ny) THEN
			aa_tdma(i) = 0.d0
			cc_tdma(i) = 0.d0
		endif
	ENDDO
	
	
	!$acc parallel loop gang vector collapse(3) private(cn,dn) default(present)
	DO npr = 1,nprims
	DO k = 1*(f2D),Nz
	DO i = 0,Nx
		
		SPt = 0;	EPt = Ny
		
		cn(SPt) = cc_tdma(SPt)
		dn(SPt) = PHI_L(i,SPt,k,npr)
		
		!$acc loop seq
		DO n = SPt+1, EPt-1
			cn(n) = cc_tdma(n) / (1.d0 - cn(n-1) * aa_tdma(n))
		ENDDO
		
		!$acc loop seq
		DO n = SPt+1, EPt
			dn(n) = (PHI_L(i,n,k,npr) - dn(n-1) * aa_tdma(n)) / (1.d0- cn(n-1) * aa_tdma(n))
		ENDDO
		
		PHI_L(i,EPt,k,npr) = dn(EPt)
		
		!$acc loop seq
		DO n = EPt-1,SPt,-1
			PHI_L(i,n,k,npr) = dn(n) - cn(n) * PHI_L(i,n+1,k,npr)
		ENDDO
		
	ENDDO
	ENDDO
	ENDDO
	!$acc end data
	
END


SUBROUTINE InterpolateZ_C5L(PHI,PHI_L)

	use global_vars
	use minmodfunctions
	implicit none
	
	integer :: n, SPt, EPt
	double precision, dimension(0:Nz) :: cn, dn
	
	double precision, dimension(Nx_min:Nx_max,Ny_min:Ny_max,Nz_min:Nz_max,nprims) :: PHI, PHI_L
	
	double precision :: alpha_a, alpha_c
	double precision, dimension(0:Nz) :: aa_tdma, cc_tdma
	
	alpha_a = 1.d0/2.d0;	alpha_c = 1.d0/6.d0
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 0,Nz
	DO j = 0,Ny
	DO i = 0,Nx
		
		IF (k>=1 .and. k<=Nz-1) THEN
			PHI_L(i,j,k,npr) = 1.d0/18.d0*PHI(i,j,k-1,npr) &
							+ 19.d0/18.d0*PHI(i,j,k+0,npr) &
							  + 5.d0/9.d0*PHI(i,j,k+1,npr)
		ELSEIF (k == 0 .or. k==Nz) THEN
			PHI_L(i,j,k,npr) = (2.d0*PHI(i,j,k-2,npr) &
							 - 13.d0*PHI(i,j,k-1,npr) &
							 + 47.d0*PHI(i,j,k+0,npr) &
							 + 27.d0*PHI(i,j,k+1,npr) &
							  - 3.d0*PHI(i,j,k+2,npr))/60.d0
		ENDIF
	
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!$acc data copyin(aa_tdma,cc_tdma)
	!$acc parallel loop default(present)
	DO i = 0,Nz
		if (i .ge. 0 .and. i .le. Nz-1) THEN
			aa_tdma(i) = alpha_a
			cc_tdma(i) = alpha_c
		elseif (i == 0 .or. i == Nz) THEN
			aa_tdma(i) = 0.d0
			cc_tdma(i) = 0.d0
		endif
	ENDDO
	
	!$acc parallel loop gang vector collapse(3) private(cn,dn) default(present)
	DO npr = 1,nprims
	DO j = 0,Ny
	DO i = 0,Nx
		
		SPt = 0;	EPt = Nz
		
		cn(SPt) = cc_tdma(SPt)
		dn(SPt) = PHI_L(i,j,SPt,npr)
		
		!$acc loop seq
		DO n = SPt+1, EPt-1
			cn(n) = cc_tdma(n) / (1.d0 - cn(n-1) * aa_tdma(n))
		ENDDO
		
		!$acc loop seq
		DO n = SPt+1, EPt
			dn(n) = (PHI_L(i,j,n,npr) - dn(n-1) * aa_tdma(n)) / (1.d0- cn(n-1) * aa_tdma(n))
		ENDDO
		
		PHI_L(i,j,EPt,npr) = dn(EPt)
		
		!$acc loop seq
		DO n = EPt-1,SPt,-1
			PHI_L(i,j,n,npr) = dn(n) - cn(n) * PHI_L(i,j,n+1,npr)
		ENDDO
		
	ENDDO
	ENDDO
	ENDDO
	!$acc end data

END


! Right face
SUBROUTINE InterpolateX_C5R(PHI,PHI_R)

	use global_vars
	use minmodfunctions
	implicit none
	
	integer :: n, SPt, EPt
	double precision, dimension(0:Nx) :: cn, dn
	
	double precision, dimension(Nx_min:Nx_max,Ny_min:Ny_max,Nz_min:Nz_max,nprims) :: PHI, PHI_R
	
	double precision :: alpha_a, alpha_c
	double precision, dimension(0:Nx) :: aa_tdma, cc_tdma
	
	alpha_a = 1.d0/6.d0;	alpha_c = 1.d0/2.d0
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 1*(f2D),Nz
	DO j = 0,Ny
	DO i = 0,Nx
		
		IF (i>=1 .and. i<=Nx-1) THEN
			PHI_R(i,j,k,npr) = 1.d0/18.d0*PHI(i+2,j,k,npr) &
							+ 19.d0/18.d0*PHI(i+1,j,k,npr) &
							  + 5.d0/9.d0*PHI(i+0,j,k,npr)
		ELSEIF (i == 0 .or. i==Nx) THEN
			PHI_R(i,j,k,npr) = (2.d0*PHI(i+3,j,k,npr) &
							 - 13.d0*PHI(i+2,j,k,npr) &
							 + 47.d0*PHI(i+1,j,k,npr) &
							 + 27.d0*PHI(i-0,j,k,npr) &
							  - 3.d0*PHI(i-1,j,k,npr))/60.d0
		ENDIF
	
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	
	!$acc data copyin(aa_tdma,cc_tdma)
	!$acc parallel loop default(present)
	DO i = 0,Ny
		if (i .ge. 0 .and. i .le. Ny-1) THEN
			aa_tdma(i) = alpha_a
			cc_tdma(i) = alpha_c
		elseif (i == 0 .or. i == Ny) THEN
			aa_tdma(i) = 0.d0
			cc_tdma(i) = 0.d0
		endif
	ENDDO
	
	!$acc parallel loop gang vector collapse(3) private(cn,dn) default(present)
	DO npr = 1,nprims
	DO k = 1*(f2D),Nz
	DO j = 0,Ny
		
		SPt = 0;	EPt = Nx
		
		cn(SPt) = cc_tdma(SPt)
		dn(SPt) = PHI_R(SPt,j,k,npr)
		
		!$acc loop seq
		DO n = SPt+1, EPt-1
			cn(n) = cc_tdma(n) / (1.d0 - cn(n-1) * aa_tdma(n))
		ENDDO
		
		!$acc loop seq
		DO n = SPt+1, EPt
			dn(n) = (PHI_R(n,j,k,npr) - dn(n-1) * aa_tdma(n)) / (1.d0- cn(n-1) * aa_tdma(n))
		ENDDO
		
		PHI_R(EPt,j,k,npr) = dn(EPt)
		
		!$acc loop seq
		DO n = EPt-1,SPt,-1
			PHI_R(n,j,k,npr) = dn(n) - cn(n) * PHI_R(n+1,j,k,npr)
		ENDDO
		
	ENDDO
	ENDDO
	ENDDO
	!$acc end data
END


SUBROUTINE InterpolateY_C5R(PHI,PHI_R)

	use global_vars
	use minmodfunctions
	implicit none
	
	integer :: n, SPt, EPt
	double precision, dimension(0:Ny) :: cn, dn
	
	double precision, dimension(Nx_min:Nx_max,Ny_min:Ny_max,Nz_min:Nz_max,nprims) :: PHI, PHI_R
	
	double precision :: alpha_a, alpha_c
	double precision, dimension(0:Ny) :: aa_tdma, cc_tdma
	
	alpha_a = 1.d0/6.d0;	alpha_c = 1.d0/2.d0
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 1*(f2D),Nz
	DO j = 0,Ny
	DO i = 0,Nx
		
		IF (j>=1 .and. j<=Ny-1) THEN
			PHI_R(i,j,k,npr) = 1.d0/18.d0*PHI(i,j+2,k,npr) &
							+ 19.d0/18.d0*PHI(i,j+1,k,npr) &
							  + 5.d0/9.d0*PHI(i,j+0,k,npr)
		ELSEIF (j == 0 .or. j==Ny) THEN
			PHI_R(i,j,k,npr) = (2.d0*PHI(i,j+3,k,npr) &
							 - 13.d0*PHI(i,j+2,k,npr) &
							 + 47.d0*PHI(i,j+1,k,npr) &
							 + 27.d0*PHI(i,j-0,k,npr) &
							  - 3.d0*PHI(i,j-1,k,npr))/60.d0
		ENDIF
	
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	
	!$acc data copyin(aa_tdma,cc_tdma)
	!$acc parallel loop default(present)
	DO i = 0,Ny
		if (i .ge. 0 .and. i .le. Ny-1) THEN
			aa_tdma(i) = alpha_a
			cc_tdma(i) = alpha_c
		elseif (i == 0 .or. i == Ny) THEN
			aa_tdma(i) = 0.d0
			cc_tdma(i) = 0.d0
		endif
	ENDDO
	
	!$acc parallel loop gang vector collapse(3) private(cn,dn) default(present)
	DO npr = 1,nprims
	DO k = 1*(f2D),Nz
	DO i = 0,Nx
		
		SPt = 0;	EPt = Ny
		
		cn(SPt) = cc_tdma(SPt)
		dn(SPt) = PHI_R(i,SPt,k,npr)
		
		!$acc loop seq
		DO n = SPt+1, EPt-1
			cn(n) = cc_tdma(n) / (1.d0 - cn(n-1) * aa_tdma(n))
		ENDDO
		
		!$acc loop seq
		DO n = SPt+1, EPt
			dn(n) = (PHI_R(i,n,k,npr) - dn(n-1) * aa_tdma(n)) / (1.d0- cn(n-1) * aa_tdma(n))
		ENDDO
		
		PHI_R(i,EPt,k,npr) = dn(EPt)
		
		!$acc loop seq
		DO n = EPt-1,SPt,-1
			PHI_R(i,n,k,npr) = dn(n) - cn(n) * PHI_R(i,n+1,k,npr)
		ENDDO
		
	ENDDO
	ENDDO
	ENDDO
	!$acc end data
END


SUBROUTINE InterpolateZ_C5R(PHI,PHI_R)

	use global_vars
	use minmodfunctions
	implicit none
	
	integer :: n, SPt, EPt
	double precision, dimension(0:Nz) :: cn, dn
	
	double precision, dimension(Nx_min:Nx_max,Ny_min:Ny_max,Nz_min:Nz_max,nprims) :: PHI, PHI_R
	
	double precision :: alpha_a, alpha_c
	double precision, dimension(0:Nz) :: aa_tdma, cc_tdma
	
	alpha_a = 1.d0/6.d0;	alpha_c = 1.d0/2.d0
	
	!$acc parallel loop gang vector collapse(4) default(present)
	DO npr = 1,nprims
	DO k = 0,Nz
	DO j = 0,Ny
	DO i = 0,Nx
		
		IF (k>=1 .and. k<=Nz-1) THEN
			PHI_R(i,j,k,npr) = 1.d0/18.d0*PHI(i,j,k+2,npr) &
							+ 19.d0/18.d0*PHI(i,j,k+1,npr) &
							  + 5.d0/9.d0*PHI(i,j,k+0,npr)
		ELSEIF (k == 0 .or. k==Nz) THEN
			PHI_R(i,j,k,npr) = (2.d0*PHI(i,j,k+3,npr) &
							 - 13.d0*PHI(i,j,k+2,npr) &
							 + 47.d0*PHI(i,j,k+1,npr) &
							 + 27.d0*PHI(i,j,k-0,npr) &
							  - 3.d0*PHI(i,j,k-1,npr))/60.d0
		ENDIF
	
	ENDDO
	ENDDO
	ENDDO
	ENDDO
	
	!$acc data copyin(aa_tdma,cc_tdma)
	!$acc parallel loop default(present)
	DO i = 0,Nz
		if (i .ge. 0 .and. i .le. Nz-1) THEN
			aa_tdma(i) = alpha_a
			cc_tdma(i) = alpha_c
		elseif (i == 0 .or. i == Nz) THEN
			aa_tdma(i) = 0.d0
			cc_tdma(i) = 0.d0
		endif
	ENDDO
	
	!$acc parallel loop gang vector collapse(3) private(cn,dn) default(present)
	DO npr = 1,nprims
	DO j = 0,Ny
	DO i = 0,Nx
		
		SPt = 0;	EPt = Nz
		
		cn(SPt) = cc_tdma(SPt)
		dn(SPt) = PHI_R(i,j,SPt,npr)
		
		!$acc loop seq
		DO n = SPt+1, EPt-1
			cn(n) = cc_tdma(n) / (1.d0 - cn(n-1) * aa_tdma(n))
		ENDDO
		
		!$acc loop seq
		DO n = SPt+1, EPt
			dn(n) = (PHI_R(i,j,n,npr) - dn(n-1) * aa_tdma(n)) / (1.d0- cn(n-1) * aa_tdma(n))
		ENDDO
		
		PHI_R(i,j,EPt,npr) = dn(EPt)
		
		!$acc loop seq
		DO n = EPt-1,SPt,-1
			PHI_R(i,j,n,npr) = dn(n) - cn(n) * PHI_R(i,j,n+1,npr)
		ENDDO
		
	ENDDO
	ENDDO
	ENDDO
	!$acc end data

END



!=========================
! POST-PROCESSING ROUTINES
!=========================

SUBROUTINE OUTPUT(task)

	use global_vars
	implicit none
	
	integer :: task
	integer, parameter :: fgrid = 1, fflow = 2
	character(len=20), external ::  str
	integer :: var
	double precision, dimension(Nx,Ny,Nz) :: X_CC_dummy, Y_CC_dummy, Z_CC_dummy
	
	print*, "Writing output file..."
	
	DO k = 1,Nz
	DO j = 1,Ny
	DO i = 1,Nx
		X_CC_dummy(i,j,k) = X_CC(i)
	    Y_CC_dummy(i,j,k) = Y_CC(j)
	    Z_CC_dummy(i,j,k) = Z_CC(k)
	ENDDO
	ENDDO
	ENDDO
	
	
	IF (task == 1) THEN
		
		open (fgrid, form='unformatted', file='grid.xyz')
			write(fgrid) Nx,Ny,Nz
			write(fgrid) 										&
			  ((( X_CC_dummy(i,j,k), i=1,Nx), j=1,Ny), k=1,Nz)	&
			, ((( Y_CC_dummy(i,j,k), i=1,Nx), j=1,Ny), k=1,Nz)	&
			, ((( Z_CC_dummy(i,j,k), i=1,Nx), j=1,Ny), k=1,Nz)
		close(fgrid)
		
		print*, 'grid.xyz file ready'
	
	ENDIF
	
	
	!$acc update self(primitive)
	
	! open (fflow, form='unformatted', file= 'Solution\\Temporal_data\\'//'flow'//trim(str(iter))//'.f')
	open (fflow, form='unformatted', file='flow.f')
		write(fflow) Nx,Ny,Nz,nprims
		write(fflow) (((( primitive(i,j,k,npr), i=1,Nx), j=1,Ny), k=1,Nz), npr=1,nprims)
	close(fflow)
	
	print*, 'flow.f file ready!'
	
	call CPU_TIME(end_time)
	Print*, 'Elapsed CPU time (secs) = ', end_time-start_time

END




